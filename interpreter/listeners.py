class InterpreterListener:
    def __init__(self):
        pass
    def started(self):
        pass
    def finished(self):
        pass
    def error(self, exception):
        pass
    def message(self, message):
        pass
    def currStatement(self, codeblock):
        pass

class ScopeListener:
    def __init__(self):
        pass
    def scopePopped(self, scope, block):
        pass
    def scopePushed(self, scope, block):
        pass
    def variableSet(self, scope, name, value):
        pass
