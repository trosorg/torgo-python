

class Scope:

    def __init__(self):
        self.stack = list()
        self.listeners = list()

    def addScopeListener(self, listener):
        if listener not in self.listeners:
            self.listeners.append(listener)

    def removeScopeListener(self, listner):
        if listener in self.listeners:
            self.listeners.remove(listener)

    def get(self, name):
        pass

    def has(self, name):
        pass

    def pop(self):
        pass

    def push(self, block):
        pass

    def set(self, name, value):
        pass

    def setNew(self, name, value):
        pass

    def getFunction(self, name):
        pass

    def hasFunction(self, name):
        pass

    def peek(self, index=0):
        return self.stack[index]

    def size(self):
        return len(self.stack)

    def __len__(self):
        return len(self.stack)

    def localVariables(self):
        pass


class LexicalScope(Scope):

    def __init__(self):
        self.scope = list()
        super().__init__()

    def get(self, name):
        p = self.stack[0]
        while p is not None:
            if p.hasVariable(name):
                return p.getVariable(name)
            p = p.getParent()
        return None

    def set(self, name, value):
        p = self.stack[0]
        while p is not None:
            if p.hasVariable(name):
                p.setVariable(name, value)
                for l in self.listeners:
                    l.variableSet(self, name, value)
                    return
            p = p.getParent()
        p = self.stack[0]
        p.setVariable(name, value)
        for l in self.listeners:
            l.variableSet(self, name, value)

    def setNew(self, name, value):
        p = self.stack[0]
        p.setVariable(name, value)
        for l in self.listeners:
            l.variableSet(self, name, value)

    def getFunction(self, name):
        p = self.stack[0]
        while p is not None:
            if p.hasFunction(name):
                return p.getFunction(name)
            p = p.getParent()
        return None

    def hasFunction(self, name):
        p = self.stack[0]
        while p is not None:
            if p.hasFunction(name):
                return True
            p = p.getParent()
        return False

    def has(self, name):
        p = self.stack[0]
        while p is not None:
            if p.hasVariable(name):
                return True
            p = p.getParent()
        return False

    def pop(self):
        block = self.stack.pop();
        for l  in self.listeners:
            l.scopePopped(self, block)
        return block

    def push(self, block):
        self.stack.insert(0, block)
        for l  in self.listeners:
            l.scopePushed(self, block)

    def localVariables(self):
        return self.stack[0].localVariables()


class DynamicScope(Scope):
    def __init__(self):
        self.scope = list()
        super().__init__()

    def push(self, block):
        self.scope.insert(0, dict())
        self.stack.insert(0, block)

        for l in self.listeners:
            l.scopePushed(self, block)

    def pop(self):
        self.scope.pop()
        block = self.stack.pop()
        for l  in self.listeners:
            l.scopePopped(self, block)
        return block

    def get(self, name):
        for s in self.scope:
            if name in s:
                return s[name]
        return None

    def set(self, name, value):
        for s in self.scope:
            if name in s:
                s[name] = value
                for l in self.listeners:
                    l.variableSet(self, name, value)
                    return
        self.scope[0][name] = value
        for l in self.listeners:
            l.variableSet(self, name, value)

    def setNew(self, name, value):
        self.scope[0][name] = value
        for l in self.listeners:
            l.variableSet(self, name, value)

    def has(self, name):
        for s in self.scope:
            if name in s:
                return True
        return False

    def getFunction(self, name):
        for s in self.stack:
            if s.hasFunction(name):
                return s.getFunction(name)
        return None

    def hasFunction(self, name):
        return getFunction(name) is not None

    def localVariables(self):
        return self.scope[0].keys()
