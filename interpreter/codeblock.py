class CodeBlock:
    def __init__(self):
        pass
    def addCommand(self, block):
        pass
    def addInterpreterListener(self, listener):
        pass
    def removeInterpreterListener(self, listener):
        pass
    def process(self, scope):
        pass
    def getCommands(self):
        pass
    def isHalted(self):
        pass
    def hasFunctions(self, name):
        pass
    def addFunction(self, function):
        pass
    def getParserRuleContext(self):
        pass
    def hasVariable(self, name):
        pass
    def getVariable(self, name):
        pass
    def localVariables(self):
        pass
    def getParent(self):
        pass
